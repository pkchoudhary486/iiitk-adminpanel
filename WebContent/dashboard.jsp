<%@ page language="java" contentType="text/html; charset=windows-1256"
	pageEncoding="windows-1256" import="com.admin.login.LoginParameters"%>
<%
	response.setHeader("Cache-Control", "no-store,must-revalidate");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", -1);
	new java.util.Date();
	if (session.getAttribute("currentFacultySessionUser") != null) {
		LoginParameters facultyUser = (LoginParameters) (session.getAttribute("currentFacultySessionUser"));
		if(facultyUser!=null){
			//out.println("User already logged in");
			request.getSession().setAttribute("fac_name",facultyUser.getName());}
		else
			request.getSession(true).setAttribute("fac_name",facultyUser.getName());	
	}
%>
<%@include file="header.jsp"%>
<%@include file="navbar.jsp"%>
<SCRIPT TYPE="text/javascript">
	var message="Sorry, right-click has been disabled";
	function clickIE() {if (document.all) {(message);return false;}}
	function clickNS(e) {if
	(document.layers||(document.getElementById&&!document.all)) {
	if (e.which==2||e.which==3) {(message);return false;}}}
	if (document.layers)
	{document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
	else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}
	document.oncontextmenu=new Function("return false")

</SCRIPT>   
<body>
<div class="container"> 
  <div class="row">
    <div class="col-lg-6"><div class="jumbotron">
			You first need to Add Subjects, Faculty, Course. 				
			<form class="form-horizontal" method="post" action="">
			<fieldset>

			<!-- Form Name -->
			<legend>Generate Time Table</legend>
			
			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="Course">Course Name</label>
			  <div class="col-md-8">
				<select id="coursefullname" name="coursefullname" class="form-control" required="">

				  
				</select>
				
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="year">Year / Session</label>  
			  <div class="col-md-8">
			  <input id="year" name="year" type="text" placeholder="" class="form-control input-md" required="">
			  <span class="help-block">Write Year e.g 2015-2016</span>  
			  </div>
			</div>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="semester">Select Semester</label>
			  <div class="col-md-8">
				<select id="semester" name="semester" class="form-control" required="">
				  <option value="one">1</option>
				  <option value="two">2</option>
				  <option value="three">3</option>
				  <option value="four">4</option>
				  <option value="five">5</option>
				  <option value="six">6</option>
				  <option value="seven">7</option>
				  <option value="eight">8</option>
				</select>
			  </div>
			</div>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="Course">Course Code</label>
			  <div class="col-md-8">
				<select id="Coursecode" name="Coursecode" class="form-control" required="">
				<?php
				// lists the course on drop down button course
				$query = $link->query("SELECT DISTINCT course_name, semester, section FROM course WHERE user_id='$user_id'"); 
				$query->setFetchMode(PDO::FETCH_ASSOC);	
				while($result = $query->fetch()){
				   echo "<option value='".$result['course_name']."'>".$result['course_name']." | ".$result['semester']." | ".$result['section']."</option>";
				}
				?>
				  
				</select>
				<span class="help-block">course, semester, section</span>  
			  </div>
			</div>

			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="generate"></label>
			  <div class="col-md-4">
				<input type="submit" id="generate" name="generate" class="btn btn-success" value="Create Time Table">
				</div>
			</div>

			</fieldset>
			</form>

		</div>
    </div>
    <div class="col-lg-6">
		<div class="jumbotron">
		<%@include file="showFaculty.jsp"%>
		</div>
    </div>
  </div>
  
</div>
	

