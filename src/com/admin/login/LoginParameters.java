package com.admin.login;

public class LoginParameters{ 
	private String name; 
	private String id;
	private String pwd;
	public boolean valid; 


	public void setId(String newId) { 
		id=newId; 
	}
	public void setName(String newName) { 
		name = newName; 
	} 

	public void setPassword(String newPassword) { 
		pwd = newPassword; 
	} 

	public String getId() { 
		return id; 
	} 
	public String getName(){ 
		return name;
	}

	public String getPassword() { 
		return pwd; 
	} 


	public boolean isValid() { 
		return valid; 
	} 

	public void setValid(boolean newValid){
		valid = newValid; 
	} 
	public void removeName() {
		name = null;
	}
	public void removeId() {
		id= null;
	}
}  
